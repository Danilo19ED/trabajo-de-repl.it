# Read a string:
# s = input()
# Print a value:
# print(s)
num_votos = {}
for _ in range(int(input())):
    candidato, votos = input().split()
    num_votos[candidato] = num_votos.get(candidato, 0) + int(votos)

for candidato, votos in sorted(num_votos.items()):
    print(candidato, votos)
